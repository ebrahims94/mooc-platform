FROM python:3.7-alpine

ARG EXTRA_REQUIREMENTS

WORKDIR /mooc-backend

ENV PATH=/opt/local/bin:$PATH \
    PYTHONPATH="$PYTHONPATH:/mooc-backend/" \
    PYTHONUNBUFFERED=1

ADD requirements.txt $EXTRA_REQUIREMENTS /mooc-backend/

RUN apk --upgrade --no-cache add bash

RUN apk --update --no-cache add \
    libpq \
    curl \ 
    gcc \
    python3 \
    python3-dev \
    libev  \
    libuv \
    && apk add --no-cache --virtual .build-deps \
    gcc \
    musl-dev \
    postgresql-dev \
    postgresql-client\
    make \
    libev-dev \
    c-ares-dev \
    libffi-dev \
    libressl-dev \
    git

RUN pip install -r /mooc-backend/${EXTRA_REQUIREMENTS:-requirements.txt}

COPY . /mooc-backend
COPY /bin/run.sh /

RUN ["chmod", "+x", "/run.sh"]

EXPOSE 5000

ENTRYPOINT [ "/run.sh" ]
