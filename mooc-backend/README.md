# MOOC-PLATFORM

Setup : 
    - you only have to setup docker & docker compose to be able to run this project.
    - Please follow this link if you don't have docker on your machine [click here.](https://docs.docker.com/engine/install/)
    - after that run ``` docker-compose up --build ``` .


Swagger :
    To navigate to swagger documentation for endpoints in this project please hit on the ``` http://localhost:5000/docs ```


# Description about solution

- User management Model with it's CRUD operations.
- Course model with it's CRUD operations.
- Category model with it's CRUD operations.
- Instructor model with it's CRUD operations.
- Dockarised backend just use docker-compose up but be careful without (sudo).
- Login API with JWT auth security layer.
- One of the APIs is protected as a POC wich is (Get users/{id}) you need to be logged in with valid token also to get data.
- Swagger documentation for all apis you can find it (localhost:5000/docs)
- Migration and structured files with documentation.
- Environment file to secure keys and so on user .sample and rename it to .env will work fine.
- Some unit tests using Pytest for endpoints and so on.
