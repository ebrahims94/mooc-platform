from mooc_platform_core.config import *
from mooc_platform_core import create_app
from extensions import db
from flask_cors import CORS

# to be able to run the app with the registered routes
app = create_app()
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI

db.app = app
db.init_app(app)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
