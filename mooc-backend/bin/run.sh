#!/bin/sh

python manage.py db init
python manage.py db migrate
python manage.py db upgrade
python manage.py db --help

python -m pytest

python3 app.py
