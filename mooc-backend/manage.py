from mooc_platform_core.config import *
from extensions import db
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI

db.init_app(app)

from mooc_platform_core.models.course_model import Course
from mooc_platform_core.models.category_model import Category
from mooc_platform_core.models.instructor_model import Instructor
from mooc_platform_core.models.users_model import Users

with app.app_context():
    db.drop_all()
    db.create_all()
    
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
