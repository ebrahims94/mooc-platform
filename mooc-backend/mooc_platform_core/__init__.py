from flask import Flask
from mooc_platform_core.apis import blueprint

def create_app():
    """
    Create the app and register all used endpoints in the registered blueprints.
    """

    app = Flask(__name__)
    app.register_blueprint(blueprint)
    
    return app
