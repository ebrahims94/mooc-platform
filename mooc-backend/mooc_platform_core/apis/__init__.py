from mooc_platform_core.apis.category import api as category_api
from mooc_platform_core.apis.course import api as course_api
from mooc_platform_core.apis.health_check import api as health_check_api
from mooc_platform_core.apis.instructor import api as instructor_api
from mooc_platform_core.apis.users import api as users_api
from mooc_platform_core.apis.login import api as login_api
from flask import Blueprint
from flask_restplus import Api

# adding blueprint with prefix to be used to add routes as a base url /api then implemented routes.
blueprint = Blueprint('api', __name__)
# adding api / endpoints used in the targeted controller and include them in the blueprint.
api = Api(
    blueprint,
    title='MOOC Platform API',
    doc='/docs',
    prefix='/api',
    version="0.0.1",
)

# adding namespace (prefix for registered routes under /api/implemented_routes)

# adding category apis
api.add_namespace(category_api, path='/category')
# adding course apis
api.add_namespace(course_api, path='/course')
# adding healthy check api
api.add_namespace(health_check_api, path='/health-check')
# adding instructor api
api.add_namespace(instructor_api, path='/instructor')
# adding users api
api.add_namespace(users_api, path='/users')
# adding login api
api.add_namespace(login_api)
