import datetime
from flask_restplus import Resource
from mooc_platform_core.swagger_models.category_model import api, category_swagger_model
from mooc_platform_core.services.category_service import CategoryService



# initialize object from CategoryService to use the service
category_service = CategoryService()


@api.route('/')
@api.doc(
    'Get All Categories',
    responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
)
class Categories(Resource):
    """
    To deal with category table in DB.
    """

    def get(self):
        """
        Handles requests to get all category.

        :returns: Success response tuple if category found otherwise return error response tuple.
        """
        try:
            results = category_service.get_all_categories()
            return {'success': 'true', 'message': 'category found successfully.', 'categories': results}, 200
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while getting category.'}, 400
    
    @api.expect(category_swagger_model)
    def post(self):
        """
        Handles requests to create new category by api payload.

        :return: category object (the one created now), or error tuple.
        """
        try:
            results = category_service.add_category(api.payload)
            return {'success': 'true', 'message': 'category created successfully.', 'category': results}, 201
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while creating category.'}, 400

@api.route('/<int:id>')
@api.doc(
    'deal with category by id',
    responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
)
class Category(Resource):
    """
    To deal with single category from DB.
    """

    def get(self, id):
        """
        Handles requests to get category by id.

        :params id: category id.
        :returns: Success response tuple if category found otherwise return error response tuple.
        """
        try:
            results = category_service.get_category(id)
            return {'success': 'true', 'message': 'category found successfully.', 'category': results}, 200
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while getting category.'}, 400

    def delete(self, id):
        """
        Handles requests to delete specific category by id.

        :params id: category id.
        :returns: Success response tuple if category marked as deleted otherwise return error response tuple.
        """
        try:
            results = category_service.update_category(id,{'deleted': True})
            return {'success': 'true', 'message': 'category deleted successfully.'}, 204
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while deleting category.'}, 400

    @api.expect(category_swagger_model)
    def put(self, id):
        """
        Handles requests to update specific category by id.

        :params id: category id.
        :returns: Success response tuple if category has been editted otherwise return error response tuple.
        """
        try:
            results = category_service.update_category(id, api.payload)
            return {'success': 'true', 'message': 'category updated successfully.'}, 200
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while updating category.'}, 400
