from datetime import datetime, date
from flask_restplus import Resource
from mooc_platform_core.services.course_service import CourseService
from mooc_platform_core.swagger_models.course_model import api, course_swagger_model


# initialize object from CourseService to use the service
course_service = CourseService()

@api.route('/')
@api.doc(
    'Get All Courses',
    responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
)
class Courses(Resource):
    """
    To deal with course table in DB.
    """

    def get(self):
        """
        Handles requests to get all courses.

        :returns: Success response tuple if course found otherwise return error response tuple.
        """
        try:
            results = course_service.get_all_courses()
            return {'success': 'true', 'message': 'Courses found successfully.', 'courses': results}, 200
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while getting courses.'}, 400

    @api.expect(course_swagger_model)
    def post(self):
        """
        Handles requests to create new courses by api payload.

        :return: courses object (the one created now), or error tuple.
        """
        try:
            if datetime.strptime(api.payload.get('starts_at'), '%Y-%m-%d') > datetime.now():
                results = course_service.add_course(api.payload)
                return {'success': 'true', 'message': 'Course created successfully.', 'course': results}, 201
            else:
                return {'success': 'false', 'message': 'Course must start in the future.'}, 400    
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while creating course.'}, 400

@api.route('/<int:id>')
@api.doc(
    'deal with courses by id',
    responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
)
class Course(Resource):
    """
    To deal with single Course from DB.
    """

    def get(self, id):
        """
        Handles requests to get course by id.

        :param id: course id.
        :returns: Success response tuple if course found otherwise return error response tuple.
        """
        try:
            results = course_service.get_course(id)
            if results:
                return {'success': 'true', 'message': 'Course found successfully.', 'course': results}, 200
            else:
                return {'success': 'false', 'message': 'Course not found.'}, 404
            
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while getting course.'}, 400

    def delete(self, id):
        """
        Handles requests to delete specific course by id.

        :param id: course id.
        :returns: Success response tuple if course marked as deleted otherwise return error response tuple.
        """
        try:
            results = course_service.update_course(id,{'deleted': True})
            return {'success': 'true', 'message': 'Course deleted successfully.'}, 204
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while deleting course.'}, 400

    @api.expect(course_swagger_model)
    def put(self, id):
        """
        Handles requests to update specific course by id and api payload.

        :param id: course id.
        :returns: Success response tuple if course has been editted otherwise return error response tuple.
        """
        try:
            results = course_service.update_course(id, api.payload)
            return {'success': 'true', 'message': 'Course updated successfully.'}, 200
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while updating course.'}, 400
