from flask_restplus import Namespace, Resource


api = Namespace('Health Check', description='Health check controller')


@api.route('/')
@api.doc(
    'Health Check',
    responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'}
)
class HealthyCheck(Resource):
    """
    To make sure app is up and running.
    """

    def get(self):
        """
        Handles requests to check app is up and running.
        :return: Success response tuple if app is up and running
                    otherwise return error response tuple.
        """
        return {'health': 'healthy'}, 200
