from flask_restplus import Resource
from mooc_platform_core.services.instructor_service import InstructorService
from mooc_platform_core.swagger_models.instructor_model import api, instructor_swagger_model


# initialize object from InstructorService to use the service
instructor_service = InstructorService()


@api.route('/')
@api.doc(
    'Get All Instructors',
    responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
)
class Instructors(Resource):
    """
    To deal with instructor table in DB.
    """

    def get(self):
        """
        Handles requests to get all instructor.

        :returns: Success response tuple if instructor found otherwise return error response tuple.
        """
        try:
            instructors = instructor_service.get_all_instructors()
            return {'success': 'true', 'message': 'Instructors found successfully.', 'instructors': instructors}, 200
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while getting instructor.'}, 400

    @api.expect(instructor_swagger_model)
    def post(self):
        """
        Handles requests to create new instructor and api payload.

        :return: instructor object (the one created now), or error tuple.
        """
        try:
            instructor = instructor_service.add_instructor(api.payload)
            return {'success': 'true', 'message': 'Instructor created successfully.', 'instructor': instructor}, 201
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while creating instructor.'}, 400

@api.route('/<int:id>')
@api.doc(
    'deal with instructor by id',
    responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
)
class Instructor(Resource):
    """
    To deal with single instructor from DB.
    """

    def get(self, id):
        """
        Handles requests to get instructor by id.

        :param id: instructor id.
        :returns: Success response tuple if instructor found otherwise return error response tuple.
        """
        try:
            instructor = instructor_service.get_instructor(id)
            return {'success': 'true', 'message': 'Instructor found successfully.', 'instructor': instructor}, 200
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while getting Instructor.'}, 400

    def delete(self, id):
        """
        Handles requests to delete specific instructor by id.

        :param id: instructor id.
        :returns: Success response tuple if instructor marked as deleted otherwise return error response tuple.
        """
        try:
            instructor = instructor_service.update_instructor(id,{'deleted': True})
            return {'success': 'true', 'message': 'Instructor deleted successfully.'}, 204
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while deleting Instructor.'}, 400

    @api.expect(instructor_swagger_model)
    def put(self, id):
        """
        Handles requests to update specific instructor by id.

        :param id: instructor id.
        :returns: Success response tuple if instructor has been editted otherwise return error response tuple.
        """
        try:
            instructor = instructor_service.update_instructor(id, api.payload)
            return {'success': 'true', 'message': 'Instructor updated successfully.'}, 200
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while updating instructor.'}, 400
