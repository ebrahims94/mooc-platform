from flask_restplus import Namespace, Resource
from mooc_platform_core.swagger_models.login_model import api, login_swagger_model
from mooc_platform_core.services.login_service import LoginService


# initialize object from LoginService to use the service
login_service = LoginService()


@api.route('/')
@api.doc(
    'Login Users',
    responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
)
class Login(Resource):
    """
    Login user to be alble to get in and hit endpoints.
    """
    
    @api.expect(login_swagger_model)
    def post(self):
        """
        Handles requests to log users in from api payload.

        :return: user object (conaints JWT token), or error tuple.
        """
        try:
            token = login_service.login_user(api.payload)
            if token:
                return {'success': 'true', 'message': 'User logged in successfully.', 'token': str(token)}, 201
            else:
                return {'success': 'false', 'message': 'There is something wrong pleae try again.'}, 400
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while creating users.'}, 400
