import datetime
from flask_restplus import Namespace, Resource, fields
from flask_restplus import reqparse
from mooc_platform_core.swagger_models.users_model import api, users_swagger_model
from mooc_platform_core.utils.decorator import token_required
from mooc_platform_core.services.user_service import UsersService


# initialize object from UserService to use the service
users_service = UsersService()


@api.route('/')
@api.doc(
    'Get All Users',
    responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
)
class Users(Resource):
    """
    To deal with users table in DB.
    """

    def get(self):
        """
        Handles requests to get all users.

        :returns: Success response tuple if user found otherwise return error response tuple.
        """
        try:
            users = users_service.get_all_users()
            return {'success': 'true', 'message': 'Users found successfully.', 'users': users}, 200
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while getting users.'}, 400
    
    @api.expect(users_swagger_model)
    def post(self):
        """
        Handles requests to create new users from api payload.

        :return: user object (the one created now), or error tuple.
        """
        try:
            user = users_service.create_user(api.payload)
            if user:
                return {'success': 'true', 'message': 'User created successfully.', 'user': user}, 201
            else:
                return {'success': 'false', 'message': 'User already registered.'}, 202
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while creating users.'}, 400

@api.route('/<int:id>')
@api.doc(
    'deal with users by id',
    responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
)
class User(Resource):
    """
    To deal with single user from DB.
    """

    @token_required
    def get(self, id):
        """
        Handles requests to get single user.

        :param id: user id.
        :returns: Success response tuple if user found otherwise return error response tuple.
        """
        try:
            user = users_service.get_user(id)
            return {'success': 'true', 'message': 'User found successfully.', 'user': user}, 200
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while getting user.'}, 400

    def delete(self, id):
        """
        Handles requests to delete specific user by id.

        :param id: user id.
        :returns: Success response tuple if user marked as deleted otherwise return error response tuple.
        """
        try:
            user = users_service.update_user(id,{'deleted': True})
            return {'success': 'true', 'message': 'User deleted successfully.'}, 204
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while deleting user.'}, 400

    @api.expect(users_swagger_model)
    def put(self, id):
        """
        Handles requests to update specific user by id and by api payload data.

        :param id: user id to be editted.
        :returns: Success response tuple if user has been editted otherwise return error response tuple.
        """
        try:
            user = users_service.update_user(id, api.payload)
            return {'success': 'true', 'message': 'User updated successfully.'}, 200
        except Exception as e:
            print(e)
            return {'success': 'false', 'message': 'something went wrong while updating user.'}, 400
