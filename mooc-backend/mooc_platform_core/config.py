import os


SECRET_KEY = os.environ.get('SECRET_KEY')
DB_HOST= os.environ.get('DB_HOST')
DB_NAME= os.environ.get('DB_NAME')
DB_USER= os.environ.get('DB_USER')
DB_PASS= os.environ.get('DB_PASS')
DB_PORT= os.environ.get('DB_PORT')
SQLALCHEMY_DATABASE_URI = 'postgresql://%s:%s@%s:%s/%s' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
