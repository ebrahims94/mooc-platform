from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from mooc_platform_core.models.category_model import Category


class CategorySchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Category
        include_relationships = True
        load_instance = True