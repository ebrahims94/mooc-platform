from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from mooc_platform_core.models.course_model import Course


class CourseSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Course
        include_relationships = True
        load_instance = True