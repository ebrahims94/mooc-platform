from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from mooc_platform_core.models.instructor_model import Instructor


class InstructorSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Instructor
        include_relationships = True
        load_instance = True