from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from mooc_platform_core.models.users_model import Users


class UserSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Users
        include_relationships = True
        load_instance = True