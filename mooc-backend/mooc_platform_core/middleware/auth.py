from mooc_platform_core.models.users_model import Users
from mooc_platform_core.utils.auth import validate_token

def get_logged_in_user(new_request):
    """
    Used to check the logged in user by validating JWT token and get whos the user

    :param request: to get Authorization header to check data
    :return: object if fail or success by verifying user 
    """
    # get the auth token
    auth_token = new_request.headers.get('Authorization')
    if auth_token:
        resp = validate_token(auth_token)
        if isinstance(resp, dict):
            user = Users.query.filter_by(id=resp.get('user_id')).first()
            response_object = {
                'status': 'success',
                'data': {
                    'user_id': user.id,
                    'email': user.email,
                    'admin': user.role,
                }
            }
            return response_object, 200
        response_object = {
            'status': 'fail',
            'message': resp
        }
        return response_object, 401
    else:
        response_object = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }
        return response_object, 401
