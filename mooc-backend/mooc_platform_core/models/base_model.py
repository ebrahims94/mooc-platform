from extensions import db
from sqlalchemy import Column, Integer, String, DateTime, Boolean, JSON, ForeignKey
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class BaseModel(db.Model, Base):
    """
    Base data model which will be shared between all models mostly.
    """

    __abstract__ = True

    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted = Column(Boolean)
