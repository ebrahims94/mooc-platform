import datetime
from mooc_platform_core.models.base_model import BaseModel
from mooc_platform_core.models.course_model import Course
from sqlalchemy import Column, Integer, String, JSON
from sqlalchemy.orm import relationship

class Category(BaseModel):
    """
    Category data model.
    """
    
    __tablename__ = "categories"

    id = Column(Integer, primary_key=True, autoincrement=True)
    category_name = Column(String(80))

    # Relationships
    course = relationship("Course", backref='categories')

    def __repr__(self):
        return '<Category %r>' % self.category_name
    
    def __init__(self, data):
        self.category_name=data.get('category_name')
        self.created_at=datetime.datetime.now()
        self.updated_at=datetime.datetime.now()
        self.deleted=data.get('deleted')

