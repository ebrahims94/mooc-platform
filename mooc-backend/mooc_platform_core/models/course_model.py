from datetime import datetime, date
from mooc_platform_core.models.base_model import BaseModel
from sqlalchemy import Boolean, Column, Date, DateTime, Float, ForeignKey, Integer, String

class Course(BaseModel):
    """
    Course data model.
    """
    
    __tablename__ = "courses"


    id = Column(Integer, primary_key=True, autoincrement=True)
    starts_at = Column(Date)
    course_name = Column(String, unique=True)
    is_active=Column(Boolean)
    duration_hours = Column(Float)
    course_description = Column(String)
    square_image = Column(String)
    instructor_id=Column(Integer, ForeignKey('instructor.id'))
    category_id = Column(Integer, ForeignKey('categories.id'))


    def __repr__(self):
        return '<Course %r>' % self.course_name
    
    def __init__(self, data):
        self.starts_at=datetime.strptime(data.get('starts_at'), '%Y-%m-%d')
        self.course_name=data.get('course_name')
        self.duration_hours=float(data.get('duration_hours'))
        self.created_at=datetime.now()
        self.updated_at=datetime.now()
        self.course_description=data.get('course_description')
        self.square_image=data.get('square_image')
        self.deleted=data.get('deleted')
        self.category_id=int(data.get('category_id'))
        self.instructor_id=int(data.get('instructor_id'))
