import datetime
from mooc_platform_core.models.base_model import BaseModel
from mooc_platform_core.models.course_model import Course
from sqlalchemy import Column, Float, Integer, String, JSON
from sqlalchemy.orm import relationship

class Instructor(BaseModel):
    """
    Instructor data model.
    """
    
    __tablename__ = "instructor"

    id = Column(Integer, primary_key=True, autoincrement=True)
    instructor_name = Column(String(80))
    instructor_rating = Column(Float)
    instructor_picture = Column(String(200))

    # Relationships
    course = relationship("Course", backref='instructor')

    def __repr__(self):
        return '<Instructor %r>' % self.instructor_name
    
    def __init__(self, data):
        self.instructor_name=data.get('instructor_name')
        self.instructor_rating=data.get('instructor_rating')
        self.instructor_picture=data.get('instructor_picture')
        self.created_at=datetime.datetime.now()
        self.updated_at=datetime.datetime.now()
        self.deleted=data.get('deleted')

