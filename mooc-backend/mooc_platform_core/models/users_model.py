
import datetime
from extensions import bcrypt
from mooc_platform_core.models.base_model import BaseModel
from sqlalchemy import Column, Integer, String, Boolean

class Users(BaseModel):
    """
    Users data model.
    """

    __tablename__ = "users"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(80))
    email = Column(String(120), unique=True)
    role = Column(String(80))
    active = Column(Boolean)
    password_hash = Column(String(100))

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<Users %r>' % self.name
    
    def __init__(self, data):
        self.name = data.get('name')
        self.email = data.get('email')
        self.role = data.get('role')
        self.active = data.get('active')
        self.password = data.get('password')
        self.created_at = datetime.datetime.utcnow()
        self.updated_at = datetime.datetime.utcnow()
        self.deleted = data.get('deleted')