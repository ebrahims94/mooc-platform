from extensions import db
import datetime
from mooc_platform_core.marshmallow_schemas.category_schema import CategorySchema
from mooc_platform_core.models.category_model import Category

class CategoryService():
    """
    CRUD operations on Category data model
    """
    
    category_schema = CategorySchema()

    def add_category(self, data):
        """
        Create new category (insert) to database with passed data.

        :param data: payload (object) contains all data to be inserted to DB.
        :return: category object which was inserted to DB.
        """

        category = Category(data)
        dumped = self.category_schema.dump(category)
        db.session.add(category)
        db.session.commit()
        return dumped

    def get_category(self, id):
        """
        Get category from DB.

        :param id: category id to be passed to the query.
        :return: category object details of the category if found if not returns None.
        """

        category = Category.query.filter_by(id=id).first() 
        return self.category_schema.dump(category)

    def get_all_categories(self):
        """
        Get all categories from database.

        :return: all categories from database.
        """

        categories = Category.query.all()
        categories = [self.category_schema.dump(category) for category in categories]
        return categories

    def update_category(self, id, data):
        """
        Delete category from DB by category id.

        :param id: category id to be deleted.
        :return: Boolean tru or false.
        """

        category = Category.query.filter_by(id=id).first()
        # make the update on category
        for key, val in data.items():
            setattr(category, key, val)
        # commit change
        db.session.commit()
