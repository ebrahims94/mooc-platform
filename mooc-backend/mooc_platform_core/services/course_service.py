from extensions import db
from mooc_platform_core.marshmallow_schemas.course_schema import CourseSchema
from mooc_platform_core.models.course_model import Course

class CourseService():
    """
    CRUD operations on Course data model
    """
    
    course_schema =  CourseSchema()

    def add_course(self, data):
        """
        Create new course (insert) to database with passed data.

        :param data: payload (object) contains all data to be inserted to DB.
        :return: course object which was inserted to DB.
        """

        # if not course:
        course = Course(data)
        dumped =  self.course_schema.dump(course)
        db.session.add(course)
        db.session.commit()
        return dumped

    def get_course(self, id):
        """
        Get course from DB.

        :param id: course id to be passed to the query.
        :return: course object details of the log if found if not returns None.
        """

        course = Course.query.filter_by(id=id).first()
        return self.course_schema.dump(course)

    def get_all_courses(self):
        """
        Get all courses from database.

        :return: all courses from database.
        """

        courses = Course.query.all()
        courses = [self.course_schema.dump(course) for course in courses]
        return courses

    def update_course(self, id, data):
        """
        Delete course from DB by course id.

        :param id: course id to be deleted.
        :return: Boolean tru or false.
        """

        course = Course.query.filter_by(id=id).first()
        # make the update on course
        for key, val in data.items():
            setattr(course, key, val)
        # commit change
        db.session.commit()
