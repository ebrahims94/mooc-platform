from extensions import db
import datetime
from mooc_platform_core.marshmallow_schemas.instructor_schema import InstructorSchema
from mooc_platform_core.models.instructor_model import Instructor

class InstructorService():
    """
    CRUD operations on Instructor data model
    """
    
    instructor_schema = InstructorSchema()

    def add_instructor(self, data):
        """
        Create new instructor (insert) to database with passed data.

        :param data: payload (object) contains all data to be inserted to DB.
        :return: instructor object which was inserted to DB.
        """

        instructor = Instructor(data)
        dumped = self.instructor_schema.dump(instructor)
        db.session.add(instructor)
        db.session.commit()
        return dumped

    def get_instructor(self, id):
        """
        Get instructor from DB.

        :param id: instructor id to be passed to the query.
        :return: instructor object details of the instructor if found if not returns None.
        """

        instructor = Instructor.query.filter_by(id=id).first() 
        return self.instructor_schema.dump(instructor)

    def get_all_instructors(self):
        """
        Get all instructors from database.

        :return: all instructors from database.
        """

        instructors = Instructor.query.all()
        instructors = [self.instructor_schema.dump(instructor) for instructor in instructors]
        return instructors

    def update_instructor(self, id, data):
        """
        Delete instructor from DB by instructor id.

        :param id: instructor id to be deleted.
        :return: Boolean tru or false.
        """

        instructor = Instructor.query.filter_by(id=id).first()
        # make the update on instructor
        for key, val in data.items():
            setattr(instructor, key, val)
        # commit change
        db.session.commit()
