from mooc_platform_core.models.users_model import Users
from mooc_platform_core.utils.auth import generate_token


class LoginService():
    """
    Login service logic
    """

    def login_user(self, data):
        """
        Login user to the system by checking credentails and generate JWT token

        :param data: dictionary contains email and password
        :return: JWT token
        """
        # fetch the user data
        user = Users.query.filter_by(email=data.get('email')).first()
        if user and user.check_password(data.get('password')):
            auth_token = generate_token(user.id, user.role, user.email)
            return auth_token
        else:
            return None

        return None
