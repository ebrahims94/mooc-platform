from extensions import db
import datetime
from mooc_platform_core.marshmallow_schemas.users_schema import UserSchema
from mooc_platform_core.models.users_model import Users

class UsersService():
    """
    CRUD operations on Users data model
    """
    
    users_schema = UserSchema()

    def create_user(self, data):
        """
        Create new user (insert) to database with passed data.

        :param data: payload (object) contains all data to be inserted to DB.
        :return: User object which was inserted to DB.
        """

        # check if user exists in DB
        user = Users.query.filter_by(email=data.get('email')).first()
        if user is None:
            user = Users(data)
            dumped = self.users_schema.dump(user)
            del dumped['password_hash']
            db.session.add(user)
            db.session.commit()
            return dumped
        
        return None

    def get_user(self, id):
        """
        Get user from DB.

        :param id: User id to be passed to the query.
        :return: Users object details of the user if found if not returns None.
        """

        user = Users.query.filter_by(id=id).first()
        user = self.users_schema.dump(user)
        del user['password_hash']
        return user

    def get_all_users(self):
        """
        Get all users from database.

        :return: all users from database.
        """

        users = Users.query.all()
        dumped_users = []
        for user in users:
            user = self.users_schema.dump(user)
            del user['password_hash']
            dumped_users.append(user)
                 
        return dumped_users

    def update_user(self, id, data):
        """
        Delete user from DB by user id.

        :param id: user id to be deleted.
        :return: Boolean tru or false.
        """
        user = Users.query.filter_by(id=id).first()
        # make the update on user
        for key, val in data.items():
            setattr(user, key, val)
        # commit change
        db.session.commit()
