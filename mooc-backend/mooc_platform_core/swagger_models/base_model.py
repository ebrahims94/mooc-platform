import datetime
from flask_restplus import fields

# create base model dictionary
base_model = {
    'created_at': fields.DateTime(required=True,
                                  default=datetime.datetime.now(),
                                  description='The Users creation date'),
    'updated_at': fields.DateTime(required=False, default=datetime.datetime.now(), description='The Users update date'),
    'deleted': fields.Boolean(required=True, default=False, description='The Users deleted or not')
}
