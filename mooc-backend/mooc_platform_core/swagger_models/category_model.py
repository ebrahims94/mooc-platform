from flask_restplus import Namespace, fields
from mooc_platform_core.swagger_models.base_model import base_model

api = Namespace('Categories', description='Category controller')

# define category model for swagger documentation
category_fields = {
    'category_name': fields.String(required=True, description='category name'),
}

#add base model fields
category_fields.update(base_model)

# define category model for swagger documentation
category_swagger_model = api.model('category', category_fields)

