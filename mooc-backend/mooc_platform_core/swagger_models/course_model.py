from datetime import date
from flask_restplus import Namespace, fields
from mooc_platform_core.swagger_models.base_model import base_model

api = Namespace('Course', description='Course controller')

# define course model for swagger documentation
course_fields = {
    'starts_at': fields.DateTime(required=True,
                                  default=date.today(),
                                  description='TCourse start date'),
    'course_name': fields.String(required=True, description='Course name'),
    'duration_hours': fields.Float(required=True, description='Course duration in hours'),
    'square_image': fields.String(required=True, description='Course image'),
    'course_description': fields.String(required=True, description='Course description'),
    'category_id': fields.Integer(required=True, description='Course category ID'),
    'instructor_id': fields.Integer(required=True, description='Course instructor ID'),
}

#add base model fields
course_fields.update(base_model)

# define course model for swagger documentation
course_swagger_model = api.model('course', course_fields)

