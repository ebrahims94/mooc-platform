from flask_restplus import Namespace, fields
from mooc_platform_core.swagger_models.base_model import base_model

api = Namespace('Instructor', description='Instructor controller')

# define instructor model for swagger documentation
instructor_fields = {
    'instructor_name': fields.String(required=True, description='instructor name'),
    'instructor_rating': fields.Float(required=True, description='instructor rating'),
    'instructor_picture': fields.String(required=True, description='instructor picture'),
}

#add base model fields
instructor_fields.update(base_model)

# define instructor model for swagger documentation
instructor_swagger_model = api.model('instructor', instructor_fields)

