from flask_restplus import Namespace, fields

api = Namespace('Login', description='Users Login')

# create users model dictionary for fields
login_fields = {
    'email': fields.String(required=True, description='The Users email'),
    'password': fields.String(required=True, description='The Users password'),
}

# define users model for swagger documentation
login_swagger_model = api.model('login', login_fields)