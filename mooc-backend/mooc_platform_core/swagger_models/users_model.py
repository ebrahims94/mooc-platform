from flask_restplus import Namespace, fields
from mooc_platform_core.swagger_models.base_model import base_model

api = Namespace('Users', description='Users controller')

# create users model dictionary for fields
user_fields = {
    'name': fields.String(required=True, description='The Users name'),
    'email': fields.String(required=True, description='The Users email'),
    'active': fields.Boolean(required=True, description='The Users active or not'),
    'password': fields.String(required=True, description='The Users password'),
    'role': fields.String(required=True, description='The Users role'),
}

#add base model fields
user_fields.update(base_model)

# define users model for swagger documentation
users_swagger_model = api.model('users', user_fields)

