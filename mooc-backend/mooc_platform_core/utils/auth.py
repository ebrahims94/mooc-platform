from mooc_platform_core.config import SECRET_KEY
import datetime
import jwt

def generate_token(user_id, role, email):
    """
    Generates the Auth Token

    :param user_id: number or integer
    :param role: string
    :param email: string
    :return: string
    """
    try:
        payload = {
            'sub': user_id,
            'role': role,
            'email': email,
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1),
            'iat': datetime.datetime.utcnow()
        }
        token = jwt.encode(
            payload,
            SECRET_KEY,
            algorithm='HS256'
        )
        return token.decode()

    except Exception as e:
        return e

def validate_token(auth_token):
    """
    Decodes the auth token to check if valid or not
    
    :param auth_token: string
    :return: integer|string
    """
    try:
        auth_token = auth_token.split(' ')[1]
        payload = jwt.decode(auth_token, SECRET_KEY)
        return {'user_id': payload.get('sub'), 'role': payload.get('role'), 'email': payload.get('email')}

    except jwt.ExpiredSignatureError:
        return 'Signature expired. Please log in again.'
    except jwt.InvalidTokenError:
        return 'Invalid token. Please log in again.'
