from flask import request
from functools import wraps
from mooc_platform_core.middleware.auth import get_logged_in_user


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        """
        Used to be on top of endpoints to require secure JWT token to pass the call
        """

        data, status = get_logged_in_user(request)
        token = data.get('data')
        
        if not token:
            return data, status

        return f(*args, **kwargs)

    return decorated