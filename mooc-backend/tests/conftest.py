import pytest
from mooc_platform_core.config import SQLALCHEMY_DATABASE_URI
from app import app
from extensions import db
from mooc_platform_core.models.users_model import Users
from mooc_platform_core.models.course_model import Course
from mooc_platform_core.models.category_model import Category
from mooc_platform_core.models.instructor_model import Instructor


@pytest.fixture
def client():
    # Create a test client using the Flask application configured for testing
    with app.test_client() as testing_client:
        # Establish an application context
        with app.app_context():
            yield testing_client  # this is where the testing happens!



@pytest.fixture
def init_database(client):
    
    # Insert user data
    user = Users({'name':'ebrahim', 'email':'admin@admin.com', 'active':True, 'password':'admin123',
                 'role':'admin', 'created_at':'2020-12-13T09:02:08.670Z', 'updated_at':'2020-12-13T09:02:08.670Z'})
    
    db.session.add(user)
    db.session.commit()

    # Insert instructor data
    instructor = Instructor({'instructor_name':'test', 'instructor_rating':8, 'instructor_picture':'https://www.google.com', 'deleted':False, 'created_at':'2020-12-13T09:02:08.670Z',
                            'updated_at':'2020-12-13T09:02:08.670Z'})

    db.session.add(instructor)
    db.session.commit()

    # Insert category data
    category = Category({'category_name':'test', 'deleted':False, 'created_at':'2020-12-13T09:02:08.670Z',
                         'updated_at':'2020-12-13T09:02:08.670Z'})

    db.session.add(category)
    db.session.commit()

    # Insert course data
    # course = Course({'starts_at':'2020-12-13','course_name':'test course','duration_hours':'60',
    #                  'deleted':False, 'course_description':'testetsettetste', 'square_image':'https://www.google.com',
    #                  'created_at':'2020-12-13T09:02:08.670Z', 'updated_at':'2020-12-13T09:02:08.670Z',
    #                  'category_id':1, 'instructor_id':1})
    
    # db.session.add(course)
    # db.session.commit()

    yield  # this is where the testing happens!

    # db.drop_all()
