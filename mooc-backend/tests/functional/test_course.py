import json


def test_post_course(client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/course' endpoint is posted to (POST)
    THEN check the response is valid

    :param client: to be able to call endpoints
    :param init_database: to make the insertion for test
    """

    response = client.post('/api/course/',
                                json={'starts_at':'2021-12-13', 'course_name':'new course', 'duration_hours':50,
                                        'deleted':False, 'course_description':'testetsettetste', 'square_image':'https://www.google.com', 'created_at':'2020-12-13T09:02:08.670Z',
                                        'updated_at':'2020-12-13T09:02:08.670Z', 'category_id':1, 'instructor_id':1})

    assert response.status_code == 201
    response = json.loads(response.get_data(as_text=True))

    assert 'true' in response.get('success')
    assert 'Course created successfully.' in response.get('message')
    assert 'course' in response


def test_invalid_course(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/course' endpoint is posted to (POST)
    THEN check the response is valid

    :param client: to be able to call endpoints
    """

    response = client.post('/api/course/',
                                json={'starts_at':'2020-12-13','course_name':'new course','duration_hours':'50',
                                        'deleted':False, 'course_description':'testetsettetste', 'square_image':'https://www.google.com', 'created_at':'2020-12-13T09:02:08.670Z',
                                        'updated_at':'2020-12-13T09:02:08.670Z','category_id':'1', 'instructor_id':'1'})

    assert response.status_code == 400
    response = json.loads(response.get_data(as_text=True))

    assert 'false' in response.get('success')
    assert 'Course must start in the future.' in response.get('message')

def test_get_courses(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/course' endpoint is posted to (GET)
    THEN check the response is valid

    :param client: to be able to call endpoints
    """

    response = client.get('/api/course/')
    assert response.status_code == 200
    response = json.loads(response.get_data(as_text=True))
    assert 'true' in response.get('success')
    assert 'Courses found successfully.' in response.get('message')
    assert 'courses' in response

def test_get_courses_by_id(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/course' endpoint is posted to (GET)
    THEN check the response is valid

    :param client: to be able to call endpoints
    """

    response = client.get('/api/course/1')
    assert response.status_code == 200
    response = json.loads(response.get_data(as_text=True))
    assert 'true' in response.get('success')
    assert 'Course found successfully.' in response.get('message')
    assert 'course' in response


def test_put_courses_by_id(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/course' endpoint is posted to (PUT)
    THEN check the response is valid

    :param client: to be able to call endpoints
    """

    response = client.put('/api/course/1',
                                json={'starts_at':'2021-12-13','course_name':'new course name','duration_hours':100,
                                        'deleted':False, 'course_description':'testetsettetste', 'square_image':'https://www.google.com', 'created_at':'2020-12-13T09:02:08.670Z',
                                        'updated_at':'2020-12-13T09:02:08.670Z','category_id':1, 'instructor_id':1})
    assert response.status_code == 200
    response = json.loads(response.get_data(as_text=True))
    assert 'true' in response.get('success')
    assert 'Course updated successfully.' in response.get('message')
