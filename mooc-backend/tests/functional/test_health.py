import json


def test_index(client):
    response = client.get('/api/health-check/')
    assert response.status_code == 200
    expected = {'health': 'healthy'}
    assert expected == json.loads(response.get_data(as_text=True))
