
import json


def test_valid_login(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/login' endpoint is posted to (POST)
    THEN check the response is valid

    :param client: to be able to call endpoints
    """

    response = client.post('/api/Login/',
                                json={'email':'admin@admin.com', 'password':'admin123'})

    assert response.status_code == 201
    response = json.loads(response.get_data(as_text=True))

    assert 'true' in response.get('success')
    assert 'User logged in successfully.' in response.get('message')
    assert 'token' in response


def test_invalid_login(client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/login' endpoint is posted to (POST)
    THEN check the response is valid

    :param client: to be able to call endpoints
    :param init_database: to make the insertion for test
    """

    response = client.post('/api/Login/',
                                json={'email':'admin@admin.com', 'password':'123'})

    assert response.status_code == 400
    response = json.loads(response.get_data(as_text=True))

    assert 'false' in response.get('success')
    assert 'here is something wrong pleae try again.' in response.get('message')
