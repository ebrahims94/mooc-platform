import React, { memo, useEffect} from "react";
import PropTypes from "prop-types";
import { Switch } from "react-router-dom";
import PropsRoute from "../../shared/components/PropsRoute";
import Home from "./home/Home";
import Blog from "./course/Blog";
import BlogPost from "./course/BlogPost";
import AddCourse from "./course/addCourse";
import EditCourse from "./course/editCourse";

function Routing(props) {

  useEffect( () => {
  }, [props.blogPosts])

  const { blogPosts, selectBlog, selectHome } = props;
  blogPosts.map(post => {
  })
  return (
    <Switch>
      {blogPosts.map((post) => (
        <PropsRoute
          path={`/courses/course/${(post || {}).course_name.replace(/\s/g, "-")}`}
          component={BlogPost}
          title={(post || {}).course_name}
          key={post.course_name.replace(/\s/g, "-")}
          src={post.square_image}
          date={post.starts_at}
          content={post.course_description}
          duration={post.duration_hours}
          id={post.id}
          otherArticles={blogPosts.filter(
            (blogPost) => blogPost.id !== post.id
          )}
        />
      ))}
      <PropsRoute
          path={'/edit-course/'}
          component={EditCourse}
      />
      <PropsRoute
          path={'/add-course/'}
          component={AddCourse}
      />
      <PropsRoute
        exact
        path="/courses"
        component={Blog}
        selectBlog={selectBlog}
        blogPosts={blogPosts}
      />
      <PropsRoute path="/" component={Home} selectHome={selectHome} />
    </Switch>
  );
}

Routing.propTypes = {
  blogposts: PropTypes.arrayOf(PropTypes.object),
  selectHome: PropTypes.func.isRequired,
  selectBlog: PropTypes.func.isRequired,
};

export default memo(Routing);
