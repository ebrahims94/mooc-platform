import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Box, isWidthUp, withWidth, withStyles, Button } from "@material-ui/core";
import TextField from '@material-ui/core/TextField';
import BlogCard from "./BlogCard";
import AddCourse from "./addCourse";


const styles = (theme) => ({
  blogContentWrapper: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(4),
      marginRight: theme.spacing(4),
    },
    maxWidth: 1280,
    width: "100%",
  },
  wrapper: {
    minHeight: "60vh",
  },
  noDecoration: {
    textDecoration: "none !important",
  },
  createCourseBtn: {
    display: "flex",
    justifyContent: "flex-end",
    marginBottom: "50px"
  }
});

function getVerticalBlogPosts(width, blogPosts) {
  const gridRows = [[], [], []];
  let rows;
  let xs;
  if (isWidthUp("md", width)) {
    rows = 3;
    xs = 4;
  } else if (isWidthUp("sm", width)) {
    rows = 2;
    xs = 6;
  } else {
    rows = 1;
    xs = 12;
  }
  blogPosts.forEach((blogPost, index) => {

    gridRows[index % rows].push(
      <Grid key={blogPost.id} item xs={12}>
        <Box mb={3}>
          <BlogCard
            src={blogPost.square_image}
            title={blogPost.course_name}
            snippet={blogPost.course_description}
            date={blogPost.starts_at}
            url={`/courses/course/${(blogPost || {}).course_name.replace(/\s/g, "-")}`}
            id={blogPost.id}
          />
        </Box>
      </Grid>
    );
  });
  return gridRows.map((element, index) => (
    <Grid key={index} item xs={xs}>
      {element}
    </Grid>
  ));
}

function Blog(props) {
  const { classes, width, blogPosts, selectBlog } = props;

  const history = useHistory();

  useEffect(() => {
    selectBlog();
  }, [selectBlog]);

  function goToAddCourse() {
    history.push('/add-course');
  }

  return (
      <Box
        display="flex"
        justifyContent="center"
        className={classNames(classes.wrapper, "lg-p-top")}
      >
        <div className={classes.blogContentWrapper}>
          <div className={classes.createCourseBtn}>
          <Button
            variant="contained"
            color="secondary"
            className={classes.extraLargeButton}
            classes={{ label: classes.extraLargeButtonLabel }}
            onClick={goToAddCourse}
          >
            Create New Course
          </Button>
          </div>
          <Grid container spacing={3}>
            {getVerticalBlogPosts(width, blogPosts)}
          </Grid>
        </div>
      </Box>
  );
}

Blog.propTypes = {
  selectBlog: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  blogPosts: PropTypes.arrayOf(PropTypes.object),
};

export default withWidth()(withStyles(styles, { withTheme: true })(Blog));
