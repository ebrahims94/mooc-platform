import React, { useEffect } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Grid, Typography, Card, Box, withStyles, Button } from "@material-ui/core";
import BlogCard from "./BlogCard";
import { useHistory } from "react-router-dom";
import ShareButton from "../../../shared/components/ShareButton";
import ZoomImage from "../../../shared/components/ZoomImage";
import smoothScrollTop from "../../../shared/functions/smoothScrollTop";

const styles = (theme) => ({
  blogContentWrapper: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(4),
      marginRight: theme.spacing(4),
    },
    maxWidth: 1280,
    width: "100%",
  },
  wrapper: {
    minHeight: "60vh",
  },
  img: {
    width: "100%",
    height: "auto",
  },
  card: {
    boxShadow: theme.shadows[4],
  },
  enrollBtn: {
    fontSize: theme.typography.body1.fontSize,
    [theme.breakpoints.up("sm")]: {
      fontSize: theme.typography.h6.fontSize,
    },
    display: "flex",
    justifyContent: "space-between",
    marginTop: "15px"
  },
  ignore: {
    overflow: "hidden"
  },
});

function BlogPost(props) {
  const { classes, date, title, src, content, otherArticles, duration, id } = props;

  useEffect(() => {
    document.title = `E-platform - ${title}`;
    smoothScrollTop();
  }, [title]);

  const history = useHistory();

  function goToEditCourse() {
    history.push({
      pathname: '/edit-course',
      state: { 
        date: date,
        title: title,
        src: src,
        content: content,
        duration_hours: duration,
        id: id
      }
    })
  }

  return (
    <Box
      className={classNames("lg-p-top", classes.wrapper)}
      display="flex"
      justifyContent="center"
    >
      <div className={classes.blogContentWrapper}>
        <Grid container spacing={5}>
          <Grid item md={9}>
            <Card className={classes.card}>
              <Box pt={3} pr={3} pl={3} pb={2}>
                <Typography variant="h4">
                  <b>{title}</b>
                </Typography>
                <Typography variant="body1" color="textSecondary">
                  {date}
                </Typography>
                <Typography variant="body1" color="textSecondary">
                  {duration} H
                </Typography>
              </Box>
              <ZoomImage className={classes.img} src={src} alt="" />
              <Box p={3}>
                <Typography className={classes.ignore} variant="body1" color="textSecondary">
                  {content}
                </Typography>
                <div className={classes.enrollBtn}>
                  <div>
                    <Button
                      variant="contained"
                      color="secondary"
                      className={classes.extraLargeButton}
                      classes={{ label: classes.extraLargeButtonLabel }}
                      onClick={goToEditCourse}
                    >
                      Edit Course
                    </Button>
                  </div>
                  <div>
                    <Button
                      variant="contained"
                      color="secondary"
                      className={classes.extraLargeButton}
                      classes={{ label: classes.extraLargeButtonLabel }}
                      href="https://programs.edraak.org/en/register/"
                      target="_blank"
                    >
                      Enroll Now
                    </Button>
                  </div>
                </div>
              </Box>
            </Card>
          </Grid>
          <Grid item md={3}>
            <Typography variant="h6" paragraph>
              Other courses
            </Typography>
            {otherArticles.map((blogPost) => (
              <Box key={blogPost.id} mb={3}>
                <BlogCard
                  title={blogPost.course_name}
                  snippet={blogPost.course_description}
                  date={blogPost.starts_at}
                  url={blogPost.course_name.replace(/\s/g, "-")}
                />
              </Box>
            ))}
          </Grid>
        </Grid>
      </div>
    </Box>
  );
}

BlogPost.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  content: PropTypes.node.isRequired,
  otherArticles: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default withStyles(styles, { withTheme: true })(BlogPost);
