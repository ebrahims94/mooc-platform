import React, {useEffect, useState, useCallback} from 'react';
import {TextField, withWidth, withStyles, TextareaAutosize, Button} from '@material-ui/core';
import courseService from "./../../../services/course_service";


const styles = (theme) => ({
  mainContainer: {
    marginTop: "150px",
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textAreaClass: {
    width: "100%",
    height: "150px !important",
  },
  submitButton: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '25px',
   }
});

function AddCourse(props) {
  const { classes } = props;

  const [name, setName] = useState("");
  const [duration, setDuration] = useState("");
  const [image, setImage] = useState("");
  const [description, setDescription] = useState("");
  const [startsAt, setStartsAt] = useState("");

  const submitCourseData = async (e) => {
    e.preventDefault();
    const data = {
      course_name: name,
      duration_hours: duration,
      square_image : image,
      course_description : description,
      starts_at : startsAt
    }
    
    const course_service = new courseService();
    const coursesData = await course_service.createCourse(data);
  }

  const handleNameChange = useCallback((e) => {
    setName(e.target.value);
  });

  const handledurationChange = useCallback((e) => {
    setDuration(e.target.value);
  });

  const handleDateChange = useCallback((e) => {
    setStartsAt(e.target.value);
  });

  const handleImageChange = useCallback((e) => {
    setImage(e.target.value);
  });

  const handleDescriptionChange = useCallback((e) => {
    setDescription(e.target.value);
  });

  useEffect(() => {
    

    // submitCourseData()
  }, [handleNameChange, handledurationChange, handleDateChange, handleImageChange, handleDescriptionChange]);

  return (
    <div className={classes.mainContainer}>
      <div>
        <h2>Create New Course</h2>
      </div>
        <form className={classes.root} noValidate autoComplete="off">
          <div>
            <div>
                <TextField
                  required
                  id="course_name"
                  label="Course Name"
                  defaultValue=""
                  InputLabelProps={{
                    shrink: true,
                  }}
                  variant="outlined"
                  onChange={handleNameChange}
                />
                <TextField
                  required
                  id="course_duration"
                  type="number"
                  label="Course Duration"
                  defaultValue=""
                  InputLabelProps={{
                    shrink: true,
                  }}
                  variant="outlined"
                  onChange={handledurationChange}
                />
              </div>
              <div>
              <TextField
                required
                id="starts_at"
                label="Starts At"
                type="date"
                defaultValue=""
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={handleDateChange}
              />
                <TextField
                  required
                  id="square_image"
                  label="Image URL"
                  defaultValue=""
                  InputLabelProps={{
                    shrink: true,
                  }}
                  variant="outlined"
                  onChange={handleImageChange}
                />
              </div>
              <div>
                <TextareaAutosize
                  className={classes.textAreaClass}
                  required
                  id="course_description"
                  label="Course Description"
                  placeholder="Descripttion goes here"
                  onChange={handleDescriptionChange}
                />
            </div>
            <div className={classes.submitButton}>
              <Button
                variant="contained"
                color="secondary"
                className={classes.extraLargeButton}
                onClick={(e) => {submitCourseData(e)}}
              >
                Create New Course
              </Button>
            </div>
          </div>
        </form>
    </div>
  );
}

export default withWidth()(withStyles(styles, { withTheme: true })(AddCourse));