import axios from "axios";

export default class courseService {
 
    createCourse = async (data) => {
        console.log('data: ', data);

        const payload = {
            course_name: data.course_name,
            starts_at: data.starts_at,
            is_active: true,
            duration_hours: data.duration_hours,
            course_description: data.course_description,
            square_image: data.square_image,
            category_id: 1,
            instructor_id: 1
        }

        const result = await axios
            .post('http://localhost:5000/api/course/',payload)
            .then((res) => {
                // console.log('res : ', res);
                return res;
            })
            .catch((error) => {
                return error;
            });
        return result;
    };

    getCourse = async (id) => {
        if (id) {
            const result = await axios
                .get('http://localhost:5000/api/course/'+id)
                .then((res) => {
                    // console.log('res: ', res);
                    return res;
                })
                .catch((error) => {
                    return error;
                });
            return result;
        }
        return "invalid course id"
    };

    getAllCourses = async () => {

        const result = await axios
            .get('http://localhost:5000/api/course/')
            .then((res) => {
                // console.log('res: ', res);
                return res;
            })
            .catch((error) => {
                return error;
            });
        return result;
    };

    updateCourse = async (course_id, data) => {

        const payload = {
            course_name: data.course_name,
            starts_at: data.starts_at,
            is_active: true,
            duration_hours: data.duration_hours,
            course_description: data.course_description,
            square_image: data.square_image,
            category_id: 1,
            instructor_id: 1
        }

        const result = await axios
            .put('http://localhost:5000/api/course/' + course_id, payload)
            .then((res) => {
                // console.log('res: ', res);
                return res;
            })
            .catch((error) => {
                return error;
            });
        return result;
    };

}