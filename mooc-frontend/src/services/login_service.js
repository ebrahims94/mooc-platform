

export default class LoginService {
    
    loginUser = async (data) => {
          if (data) {  
              const payload = {
                  email: data.email,
                  password: data.password
              }
      
              const result = await axios
                .post('http://localhost:5000/api/login/', payload)
                .then((res) => {
                    // console.log('res: ', res);
                    return res;
                })
                .catch((error) => {
                    return error;
                });
            return result;
          }
          return "invalid user credentials"
    };

}