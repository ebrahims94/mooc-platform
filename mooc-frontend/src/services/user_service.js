

export default class UserService {
    
      createUser = async (data) => {
            if (data) {  
                const payload = {
                    email: data.email,
                    password: data.password,
                    active: true,
                    name: data.name,
                    role: 'user',
                }
        
                const result = await axios
                .get('http://localhost:5000/api/users/', payload)
                .then((res) => {
                    // console.log('res: ', res);
                    return res;
                })
                .catch((error) => {
                    return error;
                });
                return result;
            }
            return "invalid user details"
      };
  
  }